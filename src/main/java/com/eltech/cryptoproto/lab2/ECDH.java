package com.eltech.cryptoproto.lab2;

import com.eltech.cryptoproto.lab2.EllCurves.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class ECDH {
    private Point publicPoint;
    private EllipticCurve curve;
    Point sharedKey;

    /**
     * Initialize ECDH exchange
     * @param c - elliptic curve
     * @param P - base point
     * @throws IllegalArgumentException if P isn't belongs to the curve
     */
    public ECDH(EllipticCurve c, Point P) throws IllegalArgumentException{
        curve=c;
        if(!curve.onCurve(P)) throw new IllegalArgumentException("This point is not on the curve!");
        publicPoint=P;
    }

    public ECDH (BigInteger a, BigInteger b, BigInteger p, BigInteger point_x, BigInteger point_y){
        this (new EllipticCurve(a,b,p),new Point(point_x,point_y));
    }

    /**
     * GOST 34-10-2012 default
     */
    public ECDH(){
        BigInteger p=new BigInteger("57896044618658097711785492504343953926634992332820282019728792003956564821041");
        long a=7;
        BigInteger b=new BigInteger("43308876546767276905765904595650931995942111794451039583252968842033849580414");
        curve=new EllipticCurve(a,b,p);
        publicPoint=new Point(2,new BigInteger("4018974056539037503335449422937059775635739389905545080690979365213431566280"));
    }

    /**
     * Set the curve to ECDH exchanger
     * @param c - curve
     */
    public void setCurve(EllipticCurve c){
        curve=c;
    }

    /**
     * Set public point to ECDH exchanger
     * @param P - point to be set
     * @throws IllegalArgumentException if P isn't belongs to the curve
     */
    public void setPublicPoint(Point P) throws IllegalArgumentException{
        if(!curve.onCurve(P)) throw new IllegalArgumentException("This point is not on the curve!");
        publicPoint=P;
    }

    /**
     * Generate public key, based on selected publicPoint
     * @param privateKey - private key number
     * @return point Q=privateKey*puvlicPoint
     */
    public Point generatePublicKey(BigInteger privateKey){
        return curve.multiply(privateKey,publicPoint);
    }

    /**
     * Shared key calculation after keys exchange
     * @param privateKey - our side private key number
     * @param otherSidePublic - other side publicKey point
     * @return
     */
    public Point calculateSharedKey(BigInteger privateKey, Point otherSidePublic){
        sharedKey = curve.multiply(privateKey, otherSidePublic);
        return sharedKey;
    }
    public Point calculateSharedKey(long privateKey, Point otherSidePublic){
        sharedKey=curve.multiply(BigInteger.valueOf(privateKey), otherSidePublic);
        return sharedKey;
    }

    /**
     * Calculate symmetric secret key, based on exchanged information
     * (max key size is 512 bits=64 bytes, because SHA-512 hashing algorithm is used)
     * @param n - byte length of encryption algorithm key
     * @throws IllegalArgumentException if requested key length is more, than 512 bits/64 bytes
     * @return byte array, containing the key
     */
    public byte[] generateCipherKey(int n) throws IllegalArgumentException{
        if (n>64) throw new IllegalArgumentException();
        byte[] key=new byte[n];
        try{
            MessageDigest hasher = MessageDigest.getInstance("SHA-512");
            key= Arrays.copyOfRange(
                    hasher.digest(sharedKey.getX().add(sharedKey.getY()).toByteArray()),
                    0,
                    n);

        } catch (NoSuchAlgorithmException e){
        }
        return key;
    }
}
