package com.eltech.cryptoproto.lab2;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Andrey Georgitsa
 * @version 1.0
 */
public class Kuznyechik_encryptor {

        private static final byte BLOCK_SIZE=16; //128 bits=16 bytes
        private ArrayList <byte[]> roundKeys=new ArrayList<>();


    /**
     * Encryptor constructor
     *
     */
        public Kuznyechik_encryptor(){

        }

        /**
         * Byte array to string conversion
         * (necessary to perform GOST examples)
         * @param bytes - byte array to be converted
         * @return string, representing the byte array
         *
         */
         public static String byteArrToStringHex(byte[] bytes){
            StringBuilder sb = new StringBuilder();
            for (byte b : bytes) {
                sb.append(String.format("%02x", b));
            }
            return sb.toString();
        }

        /**
         * String with hex numbers conversion to byte array
         * (necessary to perform GOST examples, 2 string symbols=1 byte)
         * @param str - string to be converted
         * @return byte array, containing
         * @throws IllegalArgumentException when string contains non-hex symbol
         */
        public static byte[] fromHexStrToByteArr (String str) throws IllegalArgumentException{
        int size=str.length()/2;

        if (str.length()%2!=0) {
            size++;
            StringBuilder strB=new StringBuilder();
            strB.append("0");
            strB.append(str);
            str=strB.toString();
        }

        byte[] block=new byte [size];
        for (int i = 0; i <size; i++) {
            try{
                short buf=Short.parseShort(str.substring(2*i,2*i+2),16);
                block[i]=(byte)(buf&0xFF);
            } catch (NumberFormatException ex){//not a hex symbol in str
                throw new IllegalArgumentException();
            }

        }
        return block;
        }

        /**
        * Non-linear round transformation table
        */
         private final  short[] subs_table= {
                252,  238,  221,  17,  207,  110,  49,  22,  251,  196,  250,  218,  35,  197,  4,  77,  233,
                119,  240,  219,  147,  46,  153,  186,  23,  54,  241,  187,  20,  205,  95,  193,  249,  24,  101,
                90,  226,
                92,  239,  33,  129,  28,  60,  66,  139,  1,  142,  79,  5,  132,  2,  174,  227,  106,  143,
                160,  6,  11,  237,  152,  127,  212,  211,  31,  235,  52,  44,  81,  234,  200,  72,  171,  242,  42,
                104,  162,  253,  58,  206,  204,  181,  112,  14,  86,  8,  12,  118,  18,  191,  114,  19,  71,  156,
                183,
                93,  135,  21,  161,  150,  41,  16,  123,  154,  199,  243,  145,  120,  111,  157,  158,  178,
                177,  50,  117,  25,  61,  255,  53,  138,  126,  109,  84,  198,  128,  195,  189,  13,  87,  223,
                245,  36,  169,  62,  168,  67,  201,  215,  121,  214,  246,  124,  34,  185,  3,  224,  15,  236,
                222,  122,
                148,  176,  188,  220,  232,  40,  80,  78,  51,  10,  74,  167,  151,  96,  115,  30,  0,
                98,  68,  26,  184,  56,  130,  100,  159,  38,  65,  173,  69,  70,  146,  39,  94,  85,  47,  140,  163,
                165,  125,  105,  213,  149,  59,  7,  88,  179,  64,  134,  172,  29,  247,  48,  55,  107,  228,  136,
                217,
                231,  137,  225,  27,  131,  73,  76,  63,  248,  254,  141,  83,  170,  144,  202,  216,  133,
                97,  32,  113,  103,  164,  45,  43,  9,  91,  203,  155,  37,  208,  190,  229,  108,  82,  89,  166,
                116, 210, 230, 244, 180, 192, 209, 102, 175, 194, 57, 75, 99, 182
        };
            /**
             * Non-linear inverse round transformation table
             */
            private final short[] inverse_subs_table= {
                0xA5, 0x2D, 0x32, 0x8F, 0x0E, 0x30, 0x38, 0xC0,
                0x54, 0xE6, 0x9E, 0x39, 0x55, 0x7E, 0x52, 0x91,
                0x64, 0x03, 0x57, 0x5A, 0x1C, 0x60, 0x07, 0x18,
                0x21, 0x72, 0xA8, 0xD1, 0x29, 0xC6, 0xA4, 0x3F,
                0xE0, 0x27, 0x8D, 0x0C, 0x82, 0xEA, 0xAE, 0xB4,
                0x9A, 0x63, 0x49, 0xE5, 0x42, 0xE4, 0x15, 0xB7,
                0xC8, 0x06, 0x70, 0x9D, 0x41, 0x75, 0x19, 0xC9,
                0xAA, 0xFC, 0x4D, 0xBF, 0x2A, 0x73, 0x84, 0xD5,
                0xC3, 0xAF, 0x2B, 0x86, 0xA7, 0xB1, 0xB2, 0x5B,
                0x46, 0xD3, 0x9F, 0xFD, 0xD4, 0x0F, 0x9C, 0x2F,
                0x9B, 0x43, 0xEF, 0xD9, 0x79, 0xB6, 0x53, 0x7F,
                0xC1, 0xF0, 0x23, 0xE7, 0x25, 0x5E, 0xB5, 0x1E,
                0xA2, 0xDF, 0xA6, 0xFE, 0xAC, 0x22, 0xF9, 0xE2,
                0x4A, 0xBC, 0x35, 0xCA, 0xEE, 0x78, 0x05, 0x6B,
                0x51, 0xE1, 0x59, 0xA3, 0xF2, 0x71, 0x56, 0x11,
                0x6A, 0x89, 0x94, 0x65, 0x8C, 0xBB, 0x77, 0x3C,
                0x7B, 0x28, 0xAB, 0xD2, 0x31, 0xDE, 0xC4, 0x5F,
                0xCC, 0xCF, 0x76, 0x2C, 0xB8, 0xD8, 0x2E, 0x36,
                0xDB, 0x69, 0xB3, 0x14, 0x95, 0xBE, 0x62, 0xA1,
                0x3B, 0x16, 0x66, 0xE9, 0x5C, 0x6C, 0x6D, 0xAD,
                0x37, 0x61, 0x4B, 0xB9, 0xE3, 0xBA, 0xF1, 0xA0,
                0x85, 0x83, 0xDA, 0x47, 0xC5, 0xB0, 0x33, 0xFA,
                0x96, 0x6F, 0x6E, 0xC2, 0xF6, 0x50, 0xFF, 0x5D,
                0xA9, 0x8E, 0x17, 0x1B, 0x97, 0x7D, 0xEC, 0x58,
                0xF7, 0x1F, 0xFB, 0x7C, 0x09, 0x0D, 0x7A, 0x67,
                0x45, 0x87, 0xDC, 0xE8, 0x4F, 0x1D, 0x4E, 0x04,
                0xEB, 0xF8, 0xF3, 0x3E, 0x3D, 0xBD, 0x8A, 0x88,
                0xDD, 0xCD, 0x0B, 0x13, 0x98, 0x02, 0x93, 0x80,
                0x90, 0xD0, 0x24, 0x34, 0xCB, 0xED, 0xF4, 0xCE,
                0x99, 0x10, 0x44, 0x40, 0x92, 0x3A, 0x01, 0x26,
                0x12, 0x1A, 0x48, 0x68, 0xF5, 0x81, 0x8B, 0xC7,
                0xD6, 0x20, 0x0A, 0x08, 0x00, 0x4C, 0xD7, 0x74
    };

    /**
     * Linear round transformation vector
     */
            private short[] l_vec= {

                    148,32,133,16,194,192,1,251,
                    1,192,194,16,133,32,148,1
            };


        /**
         * Substitution round transformation
         * @param block - byte array to be transformed
         * @return  byte array, containing substituted block
         *
         */
        private byte[] substitution(byte[] block){
        byte[] result=new byte [BLOCK_SIZE];
        for (int i = 0; i <BLOCK_SIZE; i++) {
            result[i]=(byte)(0xFF&subs_table[(short)(block[i]&0xFF)]);
        }
        return result;
        }
        /**
         * Inverse substitution round transformation
         * @param block - byte array to be transformed
         * @return  byte array, containing substituted block
         *
         */
        private byte[] substitutionInv(byte[] block){
        byte[] result=new byte [BLOCK_SIZE];
        for (int i = 0; i <BLOCK_SIZE; i++) {
            result[i]=(byte)(0xFF&inverse_subs_table[(short)(block[i]&0xFF)]);
        }
        return result;
        }


        /**
         * Finite field F[2^8] multiplication over
         * prime x^8+x^7+x^6+x+1 polynomial
         * @param a,b - bytes to be multiplied         *
         * @return  byte, containing multiplication result
         *
         */
        private byte  GOST_Kuz_GF_mul(byte a, short b)
        {

            int _a=(short)(a&0xFF);
            int _b=b&0xFF;
            int res=0;
            int buf=0;
            while(_b!=0){
                if ((_b& 1)!=0) res ^= _a;
                if ((_a&0x80)!=0) buf=0xC3; //0xC3 - hex value for the binary representation of given prime polynomial
                else buf=0x00;
                _a = ((_a << 1))^buf;
                _b >>= 1;
            }
            return (byte)(res&0xFF);
        }

    /**
     * Linear round transformation
     * @param block - byte array to be transformed
     * @return  byte array, containing transformed block
     *
     */
    private byte[] linearTransf(byte[] block){
                short x;
                for (int j = 0; j < 16; j++) {
                    // R transformation
                    x =(short)(block[15]&0xFF);
                    for (int i = 14; i >= 0; i--) {
                        block[i + 1] = block[i];
                        x ^= GOST_Kuz_GF_mul(block[i], l_vec[i]);
                    }
                    block[0] = (byte)(x&0xFF);
                }

            return block;
        }

    /**
     * Inverse-linear round transformation
     * @param block - byte array to be transformed
     * @return  byte array, containing transformed block
     *
     */
    private byte[] linearInvTransf(byte[] block){
        short x;
        for (int j = 0; j < 16; j++) {
            // inverse R transformation
            x =(short)(block[0]&0xFF);
            for (int i = 0; i < 15; i++) {
                block[i] = block[i+1];
                x ^= GOST_Kuz_GF_mul(block[i], l_vec[i]);
            }
            block[15] = (byte)(x&0xFF);
        }

        return block;
    }

    /**
     *
     * Key expanding constants calculation
     *
     * @param i - round number
     * @return necessary constant
     */

    private  byte[] find_C_i (int i){
        byte[] block=new byte [BLOCK_SIZE];
        block[BLOCK_SIZE-1]=(byte)(i&0xFF);
        return linearTransf(block);
    }

    /**
     * X-transformation (XOR for 2 byte blocks)
     *
     * @param a,b - blocks to be XORed
     * @return byte array, containing result
     * @throws IllegalArgumentException if blocks sizes are not equal
     */

    private byte[] xorOperation(byte[] a, byte[] b) throws IllegalArgumentException{
        if (a.length!=b.length) throw new IllegalArgumentException();
        for (int i = 0; i <BLOCK_SIZE ; i++) {
            a[i]=(byte)((a[i]^b[i])&0xFF);
        }
        return a;
    }

    /**
     * Round (iterated) keys generation
     *
     * @param key - shared key
     * @return ArrayList of 10 round keys
     */

    public ArrayList <byte[]> setKey (byte[] key){
        roundKeys.add(Arrays.copyOfRange(key,0,key.length/2));
        roundKeys.add(Arrays.copyOfRange(key,key.length/2,key.length));
        byte[] k1;
        byte[] k2;
        byte[] k_buf;

        for (int i = 1; i <=4; i++) {
            k1=roundKeys.get(2*i-2);
            k2=roundKeys.get(2*i-1);
            for (int j = 1; j <=8 ; j++) {
                k_buf=k1;
                k1=xorOperation(find_C_i((i-1)*8+j),k1);
                k1=substitution(k1);
                k1=linearTransf(k1);
                k1=xorOperation(k1,k2);
                k2=k_buf;
            }

            roundKeys.add(k1);
            roundKeys.add(k2);
        }
        return  roundKeys;
    }

    /**
     *
     * Encryption round for the given block
     *
     * @param block - plaintext block
     * @return byte array, containing encrypted block
     */
    private byte[] blockEncrypt(byte[] block){
        if (block.length>BLOCK_SIZE) throw new IllegalArgumentException();
       // block=makePadding(block);
        for (byte i=0;i<9;i++){
            //System.out.println("\nk="+byteArrToStringHex(roundKeys.get(i)));
            block=xorOperation(block,roundKeys.get(i));
           // System.out.println(byteArrToStringHex(block));
            block=substitution(block);
            //System.out.println(byteArrToStringHex(block));
            block=linearTransf(block);
            //System.out.println(byteArrToStringHex(block));
        }

        block=xorOperation(block,roundKeys.get(9));
        return block;
    }

    /**
     *
     * Decryption round for the given block
     *
     * @param block - ciphertext block
     * @return byte array, containing decrypted block
     */
    private byte[] blockDecrypt(byte[] block){
        //System.out.println("\nk="+byteArrToStringHex(roundKeys.get(9)));
        block=xorOperation(block,roundKeys.get(9));
        //System.out.println(byteArrToStringHex(block));
        for (byte i=8;i>=0;i--){
            //System.out.println("\nk="+byteArrToStringHex(roundKeys.get(i)));
            block=linearInvTransf(block);
           //System.out.println(byteArrToStringHex(block));

            block=substitutionInv(block);
            //System.out.println(byteArrToStringHex(block));

            block=xorOperation(block,roundKeys.get(i));
            //System.out.println(byteArrToStringHex(block));
        }
        return block;
    }


    /**
     *
     * Pad the given block with zeros
     *
     * @param block - block to be padded
     * @return byte array, containing padded block
     */

    private static byte[] makePadding(byte[] block){
        int to_pad_size=block.length%BLOCK_SIZE;
        if (to_pad_size==0) return block;

        byte[] result= new byte[block.length+BLOCK_SIZE-to_pad_size];
        System.arraycopy(block,0,result,0,block.length);

        return result;
    }

    /**
     * Encryption in ECB mode for
     * the whole given plaintext
     *
     * @param blocks - plaintext blocks
     * @return array of padded and encrypted blocks
     *
     */

    public byte[] encryptECB (byte[] blocks){

        blocks=makePadding(blocks);
        int blocks_count=blocks.length/BLOCK_SIZE;
        for (int i=0;i<blocks_count;i++){
            System.arraycopy(
                    blockEncrypt(Arrays.copyOfRange(blocks,BLOCK_SIZE*i,BLOCK_SIZE*(1+i))),
                    0,
                    blocks,
                    BLOCK_SIZE*i,
                    BLOCK_SIZE);
        }

        return blocks;


    }

    /**
     *  Encryption in ECB mode for the whole
     *  given plaintext using 2 threads
     *  (working faster with long arrays)
     *
     * @param blocks - plaintext blocks
     * @return array of padded and encrypted blocks
     *
     */
public byte[] parallelEncryptECB (  byte[] blocks){
    if (blocks.length==BLOCK_SIZE) {
        return encryptECB(blocks);
    }
    byte[] resBlock=makePadding(blocks);
    int blocks_count=resBlock.length/BLOCK_SIZE;

        Runnable r1=()->{
            for (int i=0;i<blocks_count;i+=2){
                System.arraycopy(
                        blockEncrypt(Arrays.copyOfRange(resBlock,BLOCK_SIZE*i,BLOCK_SIZE*(1+i))),
                        0,
                        resBlock,
                        BLOCK_SIZE*i,
                        BLOCK_SIZE);
            }
        };
        Runnable r2=()->{
            for (int i=1;i<blocks_count;i+=2){
                System.arraycopy(
                        blockEncrypt(Arrays.copyOfRange(resBlock,BLOCK_SIZE*i,BLOCK_SIZE*(1+i))),
                        0,
                        resBlock,
                        BLOCK_SIZE*i,
                        BLOCK_SIZE);
            }
        };

        Thread t1 = new Thread(r1);
        Thread t2=new Thread(r2);
        t1.start();
        t2.start();


            try{
                t1.join();
                t2.join();
            } catch (InterruptedException e){

            }

    return resBlock;

}
    /**
     * Decryption in ECB mode for
     * the whole given ciphertext
     *
     * @param blocks - ciphertext byte array
     * @return array of bytes containing decrypted blocks
     *
     */
    public byte[] decryptECB (byte[] blocks){
        int length=blocks.length;
        int blocks_count=blocks.length/BLOCK_SIZE;

        for (int i=0;i<blocks_count;i++){
            System.arraycopy(
                    blockDecrypt(Arrays.copyOfRange(blocks,0+BLOCK_SIZE*i,BLOCK_SIZE*(1+i))),
                    0,
                    blocks,
                    0+BLOCK_SIZE*i,
                    BLOCK_SIZE);
        }

        return Arrays.copyOfRange(blocks,0,length+1);
    }

    /**
     *  Decryption in ECB mode for the whole
     *  given ciphertext using 2 threads
     *  (working faster with long arrays)
     *
     * @param blocks - ciphertext blocks
     * @return array of bytes containing decrypted blocks
     *
     */
    public byte[] parallelDecryptECB (byte[] blocks){
        if (blocks.length==BLOCK_SIZE) {
            return decryptECB(blocks);
        }
        byte[] resBlock=(blocks);
        int blocks_count=blocks.length/BLOCK_SIZE;

        Runnable r1=()->{
            for (int i=0;i<blocks_count;i+=2){
                System.arraycopy(
                        blockDecrypt(Arrays.copyOfRange(blocks,BLOCK_SIZE*i,BLOCK_SIZE*(1+i))),
                        0,
                        blocks,
                        BLOCK_SIZE*i,
                        BLOCK_SIZE);
            }
        };
        Runnable r2=()->{
            for (int i=1;i<blocks_count;i+=2){
                System.arraycopy(
                        blockDecrypt(Arrays.copyOfRange(blocks,BLOCK_SIZE*i,BLOCK_SIZE*(1+i))),
                        0,
                        blocks,
                        BLOCK_SIZE*i,
                        BLOCK_SIZE);
            }
        };

        Thread t1 = new Thread(r1);
        Thread t2=new Thread(r2);
        t1.start();
        t2.start();
        try{
            t1.join();
            t2.join();
        } catch (InterruptedException e){

        }
        return Arrays.copyOfRange(blocks,0,blocks.length+1);
    }

}
