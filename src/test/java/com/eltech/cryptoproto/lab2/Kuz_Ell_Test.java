/**
 * Created by Andrey on 19.10.2018.
 */

import com.eltech.cryptoproto.lab2.*;
import com.eltech.cryptoproto.lab2.EllCurves.EllipticCurve;
import com.eltech.cryptoproto.lab2.EllCurves.Point;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;


public class Kuz_Ell_Test {

    private Kuznyechik_encryptor  e=new Kuznyechik_encryptor();

    private static final String t="There are many variations of passages of Lorem Ipsum available," +
            "but the majority have suffered alteration in some form, by injected " +
            "humour, or randomised words which don't look even slightly believable. " +
            "If you are going to use a passage of Lorem Ipsum, you need to be sure " +
            "there isn't anything embarrassing hidden in the middle of text. " +
            "All the Lorem Ipsum generators on the Internet tend to repeat predefined " +
            "chunks as necessary, making this the first true generator on the Internet. " +
            "It uses a dictionary of over 200 Latin words, combined with a handful of model " +
            "sentence structures, to generate Lorem Ipsum which looks reasonable. " +
            "The generated Lorem Ipsum is therefore always free from repetition, " +
            "injected humour, or non-characteristic words etc.";

    @Test
    public void test1_GOST3412_example(){

        String k="8899aabbccddeeff0011223344556677fedcba98765432100123456789abcdef";
        byte[] key=Kuznyechik_encryptor.fromHexStrToByteArr(k);

        String t="1122334455667700ffeeddccbbaa9988";
        byte[] txt=Kuznyechik_encryptor.fromHexStrToByteArr(t);

        e.setKey(key);

        String realRes=Kuznyechik_encryptor.byteArrToStringHex(e.encryptECB(txt));
        String expected="7f679d90bebc24305a468d42b9d4edcd";

        Assert.assertEquals(expected,realRes);
    }

    @Test
    public void test2(){
        String k="8899aabbccddeeff0011223344556677fedcba98765432100123456789abcdef";
        byte[] key=Kuznyechik_encryptor.fromHexStrToByteArr(k);
        e.setKey(key);
        byte[] txt=t.getBytes();
        byte[] enc_txt=e.encryptECB(txt);
        Assert.assertEquals(t.trim(),new String(e.decryptECB(enc_txt)).trim());
    }

    @Test
    public void test2_parallel(){
        String k="8899aabbccddeeff0011223344556677fedcba98765432100123456789abcdef";
        byte[] key=Kuznyechik_encryptor.fromHexStrToByteArr(k);
        e.setKey(key);


        byte[] txt=t.getBytes();

        byte[] enc_txt=e.parallelEncryptECB(txt);

        Assert.assertEquals(t.trim(),new String(e.parallelDecryptECB(enc_txt)).trim());
    }



    @Test
    public void test3_EllCurves(){
        BigInteger p=new BigInteger("57896044618658097711785492504343953926634992332820282019728792003956564821041");
        long a=7;
        BigInteger b=new BigInteger("43308876546767276905765904595650931995942111794451039583252968842033849580414");
        EllipticCurve alice=new EllipticCurve(a,b,p);
        EllipticCurve bob=new EllipticCurve(a,b,p);

        Point open=new Point(2,new BigInteger("4018974056539037503335449422937059775635739389905545080690979365213431566280"));

        //y^2=x^3+ax+b
        long al=81;
        long bo=125;
        Point alicePoint=alice.multiply(al,open);
        Point bobPoint=bob.multiply(bo,open);

        Point alShared=alice.multiply(al,bobPoint);
        Point bobShared=bob.multiply(BigInteger.valueOf(bo),alicePoint);

        Assert.assertEquals(alShared,bobShared);
    }

    @Test
    public void test4_binary_multiplication_check(){
        EllipticCurve curve=new EllipticCurve(16,20,47);
        Point P=new Point(15,43);
        BigInteger n=new BigInteger("1325");
        Assert.assertEquals(curve.multiply(n,P),curve.multiplySlow(n,P));
    }

    @Test  (expected = IllegalArgumentException.class)
    public void test5_ECDH_point_error(){
        ECDH exchanger=new ECDH();
        Point po=new Point(2 ,new BigInteger("123123123123123666")); //this point is not on the given curve
        exchanger.setPublicPoint(po);
    }

    @Test
    public void test6_full_protocol(){
        ECDH exchangerAlice=new ECDH();
        long alicePrivateKey=77;
        Point alicePublicKey=exchangerAlice.generatePublicKey(BigInteger.valueOf(alicePrivateKey));

        ECDH exchangerBob=new ECDH();
        long bobPrivateKey=177;
        Point bobPublicKey=exchangerBob.generatePublicKey(BigInteger.valueOf(bobPrivateKey));

        Point sharedKeyAliceSide=exchangerAlice.calculateSharedKey(alicePrivateKey,bobPublicKey);
        Point sharedKeyBobSide=exchangerBob.calculateSharedKey(bobPrivateKey,alicePublicKey);

        Assert.assertEquals(sharedKeyAliceSide,sharedKeyBobSide);

        byte[] kuzKeyAlice=exchangerAlice.generateCipherKey(32);
        Kuznyechik_encryptor alice=new Kuznyechik_encryptor();
        alice.setKey(kuzKeyAlice);
        byte[] aliceCipherText=alice.encryptECB(t.getBytes());


        byte[] kuzKeyBob=exchangerBob.generateCipherKey(32);
        Kuznyechik_encryptor bob=new Kuznyechik_encryptor();
        bob.setKey(kuzKeyBob);
        byte[] bobDecryptedText=bob.decryptECB(aliceCipherText);

        Assert.assertEquals(new String(bobDecryptedText).trim(),t);
    }

    @Test
    public void test7_full_protocol_NIST_Curve1(){
        EllipticCurve NIST_c= new EllipticCurve(-3, new BigInteger("2455155546008943817740293915197451784769108058161191238065"),new BigInteger("6277101735386680763835789423207666416083908700390324961279"));
        Point NIST_p=new Point(new BigInteger("602046282375688656758213480587526111916698976636884684818"),new BigInteger("174050332293622031404857552280219410364023488927386650641"));
        ECDH exchangerAlice=new ECDH(NIST_c,NIST_p);
        long alicePrivateKey=81;
        Point alicePublicKey=exchangerAlice.generatePublicKey(BigInteger.valueOf(alicePrivateKey));


        ECDH exchangerBob=new ECDH();
        exchangerBob.setCurve(NIST_c);
        exchangerBob.setPublicPoint(NIST_p);
        BigInteger bobPrivateKey=new BigInteger("1249");
        Point bobPublicKey=exchangerBob.generatePublicKey(bobPrivateKey);


        Point sharedKeyAliceSide=exchangerAlice.calculateSharedKey(alicePrivateKey,bobPublicKey);
        Point sharedKeyBobSide=exchangerBob.calculateSharedKey(bobPrivateKey,alicePublicKey);

        Assert.assertEquals(sharedKeyAliceSide,sharedKeyBobSide);


        byte[] kuzKeyAlice=exchangerAlice.generateCipherKey(32);
        Kuznyechik_encryptor alice=new Kuznyechik_encryptor();
        alice.setKey(kuzKeyAlice);
        byte[] aliceCipherText=alice.encryptECB(t.getBytes());


        byte[] kuzKeyBob=exchangerBob.generateCipherKey(32);
        Kuznyechik_encryptor bob=new Kuznyechik_encryptor();
        bob.setKey(kuzKeyBob);
        byte[] bobDecryptedText=bob.decryptECB(aliceCipherText);

        Assert.assertEquals(new String(bobDecryptedText).trim(),t);

    }







}
