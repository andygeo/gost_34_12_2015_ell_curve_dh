# LR2 (GOST 34.12-2015/ECDH)

Реализация блочного шифра стандарта ГОСТ Р 34.12─2015 с длиной блока 128 бит ("Кузнечик")  обмена ключами по протоколу Диффи-Хеллмана с использованием эллиптических кривых 
на языке Java для второй лабораторной работы по курсу "Криптографические протоколы" (осенний семестр 2018/2019 СПбГЭТУ).

студенты группы 4362

Георгица Андрей

Тищенко Виталия

* * *


Kuznyechik symmetric block cipher (defined in the National Standard of the Russian Federation GOST R 34.12-2015 as a cipher with block size of 128 bits and key length of 256 bits /RFC 7801) and elliptic-curve Diffie–Hellman (ECDH) key agreement protocol implementation in Java for the second lab work of сryptographic protocols course (fall semester of 2018/2019 academic year, Saint-Petersburg ETU)


Andrey Georgitsa

Tishchenko Vitaliya

* * *

Codeship: [ ![Codeship Status for andygeo/gost_34_12_2015_ell_curve_dh](https://app.codeship.com/projects/31cdaf60-c955-0136-79e1-1ebe45c38767/status?branch=master)](https://app.codeship.com/projects/314918)
